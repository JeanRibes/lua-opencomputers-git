# Pseudo-Git pour OpenComputers LUA
Permet de télécharger un repository Gitlab via HTTP.
C'est un wget sur stéroides

## Installation
```
cd /home/bin
wget https://gitlab.com/JeanRibes/lua-opencomputers-git/-/raw/master/lgit.lua
```

## Utilisation
Pour démarrer:
``lgit init <dossier> <id_gitlab> <branche>``

Ex: ``lgit init lgit-repo 24703271 master``
